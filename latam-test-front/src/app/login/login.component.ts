import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from './auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private formBuilder: FormBuilder) {   }  

    username: string;
    password : string;
    invalidLogin = false;
    loginSuccess = false;
    form: FormGroup;


  ngOnInit() {
    this.form = this.formBuilder.group({
      username: [null, Validators.required],
      password: [null, Validators.required],
    });
  }

  handleLogin() {
    
    if(this.onSubmit()){
      
      this.authenticationService.authenticationService(this.form.controls.username.value, this.form.controls.password.value).subscribe((result)=> {
        this.invalidLogin = false;
        this.loginSuccess = true;
        this.router.navigate(['/login']);
      }, () => {
        this.invalidLogin = true;
        this.loginSuccess = false;
      });
      } else{ 
      () => {
        this.invalidLogin = true;
        this.loginSuccess = false;
      }
    }
  }

  onSubmit() {
    if (this.form.valid) {
      console.log('form submitted');
      return true;
    } else {
      console.log('form is not valid');
      //todo: mostrar mensaje en pantalla not working
      Object.keys(this.form.controls).forEach(field => { // {1}
        const control = this.form.get(field);            // {2}
        control.markAsTouched({ onlySelf: true });       // {3}
      });    
    return false;
    }
  }
}
